#!/bin/bash

TAG=15.0
IMAGE_NAME=warzon/distcc-ndk

docker build --no-cache -t="$IMAGE_NAME" .
docker tag $IMAGE_NAME docker.io/$IMAGE_NAME:$TAG
docker push docker.io/$IMAGE_NAME:$TAG
