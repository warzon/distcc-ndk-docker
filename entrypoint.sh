#!/bin/bash -e

export PATH=$TOOLCHAIN_PATH:$PATH
distccd --no-detach --daemon --allow 10.100.0.0/16 --allow 127.0.0.1 --log-stderr --verbose -p $DISTCC_PORT
