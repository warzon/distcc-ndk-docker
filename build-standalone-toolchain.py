#!/usr/bin/env python
import os
import sys
import shutil
sys.path.append("./build/tools/")
import make_standalone_toolchain

def main():
    ndk_path = os.path.dirname(os.path.realpath(__file__))
    version = '4.9'
    arches = ['arm','arm64','mips','mips64','x86','x86_64']
    api = '19'
    sys.argv.append('--stl=libc++')
    sys.argv.append('-v')
    sys.argv.append('--force')

    basic_args = sys.argv
    for arch in arches:
        sys.argv = basic_args
        if arch == 'arm' or arch == 'x86':
            api = '19'
        else:
            api = '21'

        toolchain = {
            'arm': 'arm-linux-androideabi',
            'arm64': 'aarch64-linux-android',
            'mips': 'mipsel-linux-android',
            'mips64': 'mips64el-linux-android',
            'x86': 'x86',
            'x86_64': 'x86_64',
        }[arch] + '-' + version

        install_dir = ndk_path + '/../toolchains/' + toolchain + '/'
        if not os.path.exists(install_dir):
            os.makedirs(install_dir)
        sys.argv.append('--arch=' + arch)
        sys.argv.append('--api=' + api)
        sys.argv.append('--install-dir=' + install_dir)

        print '**********************'
        print 'building ' + toolchain + '-' + version
        print '**********************'
        make_standalone_toolchain.main()

        # this fix allows not to have dependancy from libc++_shared 
        triple = make_standalone_toolchain.get_triple(arch)
        for abi in make_standalone_toolchain.get_abis(arch):
            dest_libdir = make_standalone_toolchain.get_dest_libdir(install_dir, triple, abi)
            os.remove(os.path.join(dest_libdir, 'libstdc++.a'))
            os.remove(os.path.join(dest_libdir, 'libstdc++.so'))
            shutil.move(os.path.join(dest_libdir, 'libc++_static.a'), os.path.join(dest_libdir, 'libstdc++.a'))

        # fix for CentOS
        os.remove(os.path.join(install_dir, 'bin', 'python2.7'))
        os.remove(os.path.join(install_dir, 'bin', 'python2'))
        os.remove(os.path.join(install_dir, 'bin', 'python'))

if __name__ == '__main__':
    main()
