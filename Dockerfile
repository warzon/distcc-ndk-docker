FROM        ubuntu:17.04
MAINTAINER  Michael Tsukerman <miketsukerman@gmail.com>

ENV         REFRESHED_AT 2016-06-20
ENV         DEBIAN_FRONTEND noninteractive
ENV         ANDROID_NDK_HOME /opt/android-ndk

RUN         dpkg --add-architecture i386 && \
            apt-get update && apt-get upgrade -y && apt-get install -y git wget && \
            wget -O - http://apt.llvm.org/llvm-snapshot.gpg.key|apt-key add - && apt-get update && \
            apt-get install -y --no-install-recommends software-properties-common pkg-config && \
            add-apt-repository -y ppa:george-edison55/cmake-3.x && \
            apt-get install -y python libtclap-dev libz-dev locales unzip && \
            apt-get install -y --no-install-recommends --no-install-suggests g++ clang distcc && \
            apt-get install -y --no-install-recommends cmake make patch && \
            apt-get -q -y clean && rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/* && \
            locale-gen en_US.UTF-8 && dpkg-reconfigure locales && \
            echo "user:x:1000:0:user,,,:/home:/bin/bash" >> /etc/passwd && echo "user::1000:0:user,,,:/home:/bin/bash" >> /etc/shadow && chmod a+wx /home

ADD         build-standalone-toolchain.py       /opt/build-standalone-toolchain.py
RUN         chmod +x /opt/build-standalone-toolchain.py

# download
RUN         mkdir /opt/android-ndk-tmp
RUN         cd /opt/android-ndk-tmp && wget -q https://dl.google.com/android/repository/android-ndk-r15-linux-x86_64.zip

RUN         cd /opt/android-ndk-tmp && unzip ./android-ndk-r15-linux-x86_64.zip
RUN         mv /opt/build-standalone-toolchain.py /opt/android-ndk-tmp/android-ndk-r15
RUN         cd /opt/android-ndk-tmp/android-ndk-r15 && ./build-standalone-toolchain.py

RUN         cd /opt/android-ndk-tmp && mv ./toolchains /opt/ndk-toolchains

# remove temp dir
RUN         rm -rf /opt/android-ndk-tmp


# add to PATH
ENV         PATH ${PATH}:${ANDROID_NDK_HOME}

ENV         LANG en_US.UTF-8
ENV         LC_ALL en_US.UTF-8
ENV         HOME /home/

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD         env
