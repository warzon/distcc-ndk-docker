#!/bin/bash

set -e
set -o nounset

USAGE="usage:\n
	-a | --arch [arch]           arm | x86 | x86_64 | arm64\n
	-p | --port                	 distcc port \n
	-h | --help                  this help mesage"

OPTS=`getopt -o a:p:h --long architecture:,port:,help -- "$@"`
eval set -- "$OPTS"

ARCH="arm"
TOOLCHAIN_PATH=""
DISTCC_PORT=""
TOOLCHAIN_BASE_PATH="/opt/ndk-toolchains"
ANDROID_TOOLCHAIN_VERSION="4.9"

while true; do
	case "$1" in
		-a | --architecture )
		ARCH="$2";
		shift; shift ;;
	-p | --port )
		DISTCC_PORT="$2";
		shift ;;
	-h | --help )
		shift;
		echo -e $USAGE;
		exit 1 ;;
	-- )
		shift;
		break ;;
	* ) break ;;
	esac
done

echo "ARCH : $ARCH"

if [ $ARCH = "arm64" ]; then
	TOOLCHAIN_BASE_PATH="$TOOLCHAIN_BASE_PATH/aarch64-linux-android-$ANDROID_TOOLCHAIN_VERSION"
	TOOLCHAIN_PATH="$TOOLCHAIN_BASE_PATH/bin:$TOOLCHAIN_BASE_PATH/aarch64-linux-android/bin"
elif [ $ARCH = "x86" ]; then
	TOOLCHAIN_BASE_PATH="$TOOLCHAIN_BASE_PATH/x86-${ANDROID_TOOLCHAIN_VERSION}"
	TOOLCHAIN_PATH="$TOOLCHAIN_BASE_PATH/bin:$TOOLCHAIN_BASE_PATH/i686-linux-android/bin"
elif [ $ARCH = "x86_64" ]; then
	TOOLCHAIN_BASE_PATH="$TOOLCHAIN_BASE_PATH/x86_64-${ANDROID_TOOLCHAIN_VERSION}"
	TOOLCHAIN_PATH="$TOOLCHAIN_BASE_PATH/bin:$TOOLCHAIN_BASE_PATH/x86_64-linux-android/bin"
elif [ $ARCH = "arm" ]; then
	TOOLCHAIN_BASE_PATH="$TOOLCHAIN_BASE_PATH/arm-linux-androideabi-$ANDROID_TOOLCHAIN_VERSION"
	TOOLCHAIN_PATH="$TOOLCHAIN_BASE_PATH/bin:$TOOLCHAIN_BASE_PATH/arm-linux-androideabi/bin"
else
	echo "Architecture $ARCH is not supported"
	exit 1	
fi

echo "PORT : $DISTCC_PORT"
echo "TOOLCHAIN_PATH : $TOOLCHAIN_PATH"

CONTAINER_NAME=distcc-ndk
IMAGE_NAME=docker.io/warzon/$CONTAINER_NAME
TAG=15.0

sudo docker stop $CONTAINER_NAME
sudo docker rm -f $CONTAINER_NAME &> /dev/null
sudo docker pull $IMAGE_NAME:$TAG
sudo docker run --name $CONTAINER_NAME --net host -e TOOLCHAIN_PATH=$TOOLCHAIN_PATH -e DISTCC_PORT=$DISTCC_PORT -i $IMAGE_NAME:$TAG
